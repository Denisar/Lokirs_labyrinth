﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

public class Health : MonoBehaviour {

	[SerializeField]
	private Slider hpSlider;
	[SerializeField]
	private int startingHealth;

    private int currentHealth;
	private bool isDead;
	private float timeBetweenTakingDamage = 0;
	private const float dmgTimer = 10;
	private bool isDashing = false;


	public int GetHealth(){return currentHealth;}
	public bool Status(){return isDead;}
	public bool IsDashing {
		get{return isDashing;}
		set{isDashing = value;}
		}

	void Awake(){
		currentHealth = startingHealth;
		isDead = false;
		if(startingHealth == 0)
		Debug.Log("Starting health in health is 0 " +  this.gameObject.name);
		
	}
	void Start()
	{
		if(this.tag == "Player"){
			hpSlider.maxValue = startingHealth;
		}
	}

	void FixedUpdate()
	{
		if(this.tag == "Player"){
			hpSlider.value = currentHealth;
		}
		
		if(currentHealth > startingHealth)
			currentHealth = startingHealth;


		if(timeBetweenTakingDamage >= 0){
			timeBetweenTakingDamage--;
			this.GetComponent<SpriteRenderer>().color = Color.red;
		}else{
			this.GetComponent<SpriteRenderer>().color = Color.white;
		}
	}
	public void pAttacked(int dmg){
		if(!isDashing){
			if(timeBetweenTakingDamage <= 0){
				currentHealth -= dmg;
				if(currentHealth <= 0 && !isDead)
       			{
       	    	 Dead ();
       			}
			timeBetweenTakingDamage = dmgTimer;
			}
		}

		
	}
	public void HealPlayer(int heal){
		currentHealth += heal;
	}
	
	void Dead(){
		isDead = true;
		Destroy(this.gameObject);
		//This is where the death animation goes 
	}
	
}
