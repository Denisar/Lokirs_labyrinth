﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pMovement : MonoBehaviour {
    
	public LayerMask pMask,pMask2,ropeMask;

	[SerializeField]
	private float speed , jumpHeight;
	[SerializeField]
	private int dashSpeed = 50;
	[SerializeField]
	private bool isAttacking;
	
	private Rigidbody2D pRB;
	private Transform pPosition, tGround, tWall, tRope;
	private Health playerHealth;
	private float facing = 0.15207f;
	private float fallMultiplier = 5f;
	private float lowJumpMultiplier = 2f;
	private float maxFallSpeed = -40;
	private float airTime = 0;
	private float minFallTime = 0.6f;
	private float damageForSeconds = 10f;
	private int fallDamageTaken;
	private bool isFacingRight;//this is made for projectiles
	private bool isDashing;//this determines whether the collider is on.
	private bool isJumping;//this will be used when playe presses jump
	private bool isWallClimbing;//checks whether the player can climb up a wall
	private bool isClimbing;//this is done to check if the player can jump 
	private bool isGrounded = false;
	private bool isRopeClimbing;
	private bool isBlocking=false;
	private bool isHolding=false;

	public bool Attacking{get{return isAttacking;}set{isAttacking = value;}}

	private void Start () {
		pRB = gameObject.GetComponent<Rigidbody2D> ();
		pPosition = this.transform;
		tGround = GameObject.Find(this.name+"/tGround").transform;
		tWall = GameObject.Find(this.name+"/tWallClimb").transform;
		tRope = GameObject.Find(this.name+"/tRopeClimb").transform;
		playerHealth = GetComponent<Health>();
	}
	
	void Update()
	{
		if(Input.GetButtonDown("Jump")&& isGrounded ){
			isJumping = true;
			isHolding = true;
		}
		playerHealth.IsDashing = isDashing;
	}

	private void FixedUpdate () {
		FallDamage();
		Movement();
		Dashing();
		Checker();
		jumping();
	}

	private void Movement(){
		if(!isBlocking){
			Move (Input.GetAxisRaw ("Horizontal"));
			pRB.constraints = RigidbodyConstraints2D.FreezeRotation;
		}else{
			Move(0);
			pRB.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
		}
		if(isWallClimbing && (GetComponentInChildren<WeaponManager>().SelectedWeapon == 1 || isClimbing)){
			Climb(Input.GetAxisRaw ("Vertical"));
		}
	}

	private void Checker(){
		isGrounded = Physics2D.Linecast (pPosition.position,tGround.position,pMask);
		isRopeClimbing = Physics2D.OverlapBox(tRope.position,new Vector2(1,1),0,ropeMask);
		isWallClimbing = Physics2D.OverlapBox(tWall.position,new Vector2(1,1),0,pMask2);
		if(isWallClimbing || isRopeClimbing){
			isGrounded = true;
		}
		if(isHolding){
			isRopeClimbing = isWallClimbing = false;
		}
	}

	private void jumping(){

		if(Input.GetButtonUp("Jump")&&isHolding&&!isGrounded){
			pRB.velocity = new Vector2(pRB.velocity.x, pRB.velocity.y - 20);
			isHolding = false;
		}
		if(isJumping)
			Jump();
			isJumping = false;
		
		ImprovedGravity();
	}
	

	private void Dashing(){
		if(isDashing && isFacingRight){
			pRB.velocity = new Vector2(dashSpeed,0);
			Physics2D.IgnoreLayerCollision(10,12,true);
			pRB.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
		}else if(isDashing && !isFacingRight){
			pRB.velocity = new Vector2(-dashSpeed,0);
			Physics2D.IgnoreLayerCollision(10,12,true);
		}else{
			pRB.constraints = RigidbodyConstraints2D.FreezeRotation;
			Physics2D.IgnoreLayerCollision(10,12,false);
		}
	}
	private void FallDamage(){
		if(!isGrounded){
			airTime += Time.deltaTime;
		}
		if(isGrounded){
			if(airTime > minFallTime){
				fallDamageTaken = (int)(damageForSeconds * airTime);
				GetComponent<Health>().pAttacked(fallDamageTaken);
			}
			airTime = 0;
		}
	}
	
	private void Move(float h){
		Vector2 pVelocity = pRB.velocity;
		pVelocity.x = h * speed * Time.deltaTime;
		pRB.velocity = pVelocity;
		Flip(h);
	}

	public void Climb(float v){
		Vector2 pVelocity = pRB.velocity;
		pVelocity.y = v * speed * Time.deltaTime;
		pRB.velocity = pVelocity;
	}
	
	public void setGrounded(){
		isGrounded = true;
	}

	private void Jump(){
		Vector2 temp = pRB.velocity;
		temp = Vector2.up*jumpHeight;
		pRB.velocity = new Vector2(pRB.velocity.x,temp.y);
	}
	
	public bool Facing(){
		return isFacingRight;
	}

	private void Flip(float pFacing){
		if(pFacing < -0.1 && !isAttacking){
			transform.localScale = new Vector3(-facing,transform.localScale.y,transform.localScale.z);
			isFacingRight = false;
		}else if(pFacing > 0.1 && !isAttacking){
			transform.localScale = new Vector3(facing,transform.localScale.y,transform.localScale.z);
			isFacingRight = true;
		}
	}
	
	public bool pBlocking{
		get {return isBlocking;}
		set {isBlocking = value;}
	}

	private void ImprovedGravity(){
		Vector2 vel = pRB.velocity;
		if(pRB.velocity.y < maxFallSpeed)//Terminal Velocity
         {
                vel = Vector2.up * maxFallSpeed;
         }
		if(pRB.velocity.y < 0){
			vel += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1)*Time.fixedDeltaTime;
			isHolding = false;
		}else if(pRB.velocity.y > 0 && !isHolding){
			vel += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1)*Time.fixedDeltaTime;
		}
		pRB.velocity = new Vector2(pRB.velocity.x,vel.y);
	}
	public void Dash(){
		isDashing = true;
	}
	public void ResetDash(){
		isDashing = false;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == "Rope"){
			isClimbing = true;
		}
	}
	void OnTriggerExit2D(Collider2D other)
	{
		if(other.gameObject.tag == "Rope"){
			isClimbing =false;
		}
	}
}
