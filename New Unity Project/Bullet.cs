﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	[SerializeField] int bulletSpeed = 50;//This is the speed that the bullet travels at
	[SerializeField] int executePercent;
	private int Damage;//Damage that is set by the player or enemy
	private bool triggered = false;//Has the trigger gone off
	private GameObject firer;//The gameobject that shot the bullet
	private bool Altfire = false;
	
	void FixedUpdate () {
		transform.Translate(Vector3.right*Time.deltaTime*bulletSpeed);//This will make it so the bullet moves forward constantly
		Destroy(gameObject,1f);//This will destroy this instance of the bullet after 1 second on screen
		
	}
	public void SetDamage(int amount){//Sets how much damage the bullet will do
		Damage = amount;
	}
	public void SetFirer(GameObject whoFired){//Sets who fired the bullet
		firer = whoFired;
	}
	void OnTriggerExit2D(Collider2D other)
	{
		//I did it this way cause the player was hitting himself alot if i didn't set it like this, Also this mean we can use one script for bullet
		if(triggered == false){//This is to make sure a single bullet doesnt hit twice 
			triggered = true;
			if(firer.tag == "Player"&& other.gameObject.tag =="Enemy"){//if the bullet hit enemy shot from player 
				EnemyHealth enemyHealth = other.GetComponent <EnemyHealth> ();
				if(Altfire == true && enemyHealth.GetPercentHealth() <= executePercent){
					enemyHealth.IsExecuted();
				}
				 Destroy(gameObject);
           	     enemyHealth.TakeDmg(Damage);//Does damage based on the amount set
			}else if(firer.tag == "Enemy"&& other.gameObject.tag =="Player"){//if the bullet hit player shot from enemy
				PlayerHealth playerHealth = other.GetComponent <PlayerHealth> ();
				 Destroy(gameObject);
           	     playerHealth.Attacked(Damage);//Does damage based on the amount set
			}else if(other.gameObject.tag == "Room"||other.gameObject.tag == "Wall"){//if the bullet hit the wall or leaves the room
				Destroy(gameObject);//This will delete the bullet instance if it hits any object with a box collider
			}else if(firer.tag == "Enemy"&& other.gameObject.tag =="Enemy"){
				triggered = false;
			}

		}
	}

}	

