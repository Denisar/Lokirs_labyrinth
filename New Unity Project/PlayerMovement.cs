﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
	[SerializeField] float speed = 10f;//This is the movement speed for the player
	private Rigidbody2D playerRBody;
	private Vector3 movement;
	private Camera cameraMain;
 	private Transform transformPlayer;
	void Awake(){
		cameraMain = Camera.main;
		transformPlayer = GetComponent <Transform> ();
		playerRBody = GetComponent<Rigidbody2D>();
	}
	void FixedUpdate()
	{//This will allow us to have controller controls by taking the raw input built in unity
		float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        Move(h,v);
		
		turning();
	}
	//This is basic movemnet W,A,S,D
	void Move(float h, float v){
		 movement.Set (h,v,0f);
        movement = movement.normalized *speed*Time.deltaTime;
        playerRBody.MovePosition(transform.position+movement);
	}
	//This method will turn the charater to be facing the mouse
	void turning(){
	float camDistance = cameraMain.transform.position.y - transformPlayer.position.y;
    Vector3 mouse = cameraMain.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, camDistance));
    float AngleRad = Mathf.Atan2 (mouse.y - transformPlayer.position.y, mouse.x - transformPlayer.position.x);
    float angle = (180 / Mathf.PI) * AngleRad;
    playerRBody.rotation = angle;
	}
}
	
	
	
	

