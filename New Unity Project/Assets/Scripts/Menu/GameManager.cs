﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	[SerializeField]
	private GameObject BossDoor;
	[SerializeField]
	private GameObject Boss;
	[SerializeField]
	private int PlayerLives = 3;
	[SerializeField]
	private Text txt;
	
	public static GameManager instance;
	private GameObject player;

	void Start () {
		if(instance == null){
				instance = this;
			}else if(instance != this){
				Destroy(this.gameObject);
			}
		DontDestroyOnLoad(this.gameObject);
	}
	
	
	void Update () {
		if(Input.GetKeyDown(KeyCode.H)){
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
		}
		if(Input.GetButtonDown("Cancel")){
			Application.Quit();
		}
		FindPlayer();
		Reset();
		if(SceneManager.GetActiveScene().name != "End Game" && SceneManager.GetActiveScene().name != "Menu Scene"){
			if(player == null ){
				SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
				PlayerLives--;
			}else if(PlayerLives <= 0 ){
				SceneManager.LoadScene("End Game");
			}
		}
		if(player != null && Boss == null){
			BossDoor.SetActive(true);
		}
	}
	private void FindPlayer(){
		if(SceneManager.GetActiveScene().name != "End Game" && SceneManager.GetActiveScene().name != "Menu Scene" && player == null){
			player = GameObject.FindGameObjectWithTag ("Player");
			txt = GameObject.FindGameObjectWithTag("EditorOnly").GetComponent<Text>();
			txt.text = "Lives: "+PlayerLives.ToString();
		}
	}
	private void Reset(){
		if(SceneManager.GetActiveScene().name == "Menu Scene")
			PlayerLives = 3;
	}
}
