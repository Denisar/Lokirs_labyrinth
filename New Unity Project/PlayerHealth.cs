﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour {

	[SerializeField] float defensiveTimeLimit;
	private int startingHealth = 100;
    private int currentHealth;
    public Slider healthSlider;
	public Image damageImage;
	PlayerAbility playerAbility;

	private int shieldSize;	
	private float defensiveTimer;
	private bool playerHit;
	private bool isDead;
	private bool defensiveActive = false;
	private bool absorb = false;
	
	

	void Awake()
	{
		//Initialize variables 
		currentHealth = startingHealth;
		playerAbility = GetComponent<PlayerAbility>();
	}
	
	void FixedUpdate()
	{
		defensiveTimer += Time.deltaTime;//Real time timer
		if(defensiveActive == true || absorb == true && defensiveTimer >= defensiveTimeLimit && Time.timeScale != 0){
			//This will wait until the buff has been on the player for a set amount of time and it is active before running
			shieldSize = 0;//This reset the shield
			DefensiveDeactivate();//This turns off all the defensive buffs
		}
	}
	
	public void Attacked(int amount){
		//This will happen everytime the player is hit
		//playerHit = true; 
		if(absorb == true){//This checks is absorb is on
			amount = -amount;//This will convert the dmg to a negative so when its taken away from hp it increases hp instead.
		}

		if(shieldSize>0 && shieldSize>amount){//this will check if there is a shield still
			shieldSize -= amount;
		}else if(shieldSize>0 && shieldSize<=amount){//this will be used when shield is about to break or broke
			shieldSize-=amount;
			currentHealth += shieldSize;
			DefensiveDeactivate();//This will deactive buff. only one defensive can be active at once so it wont affect absorb
			playerAbility.ShieldBroke();
		}else{//if no shield up
			currentHealth -= amount;
			healthSlider.value = currentHealth;
		}
		if(currentHealth <= 0 && !isDead)
        {
			//here we add any death sound
            Dead ();
        }
	}
	void Dead(){
		isDead = true;
		//This is where we put a death animation 
	}
	public void Shield(int shieldAmount){//give a sheild
		shieldSize = shieldAmount;
		defensiveActive = true;//This is so we know that the shield is active
		defensiveTimer = 0f;//This is to reset the timer
	}
	public void Absorb(){//this will be activated when defensive skill 2 is used 
		absorb = true;//Sets a bool to true so that dmg is healed
		defensiveTimer = 0f;//Time resetter
	}
	public int GetHealth(){//Send over how much health the player has
		return currentHealth;
	}
	void DefensiveDeactivate(){//This turns off all defensive buffs that are currently on
		defensiveActive = false;
		absorb = false;
	}
	
	
	
}
