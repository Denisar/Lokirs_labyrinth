﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonAttack : MonoBehaviour {

	[SerializeField] 
	private int damage;
	[SerializeField] 
	private float distanceFromPlayer;
	
	private GameObject player;
	private Animation sAnimation;
	private eMovement enemyMovement;
	private float timer = 0f;
	private float timeBetweenAttacks = 2f;
	private bool isAttacking;
	private float distance;

	void Awake()
	{
		sAnimation = GetComponent<Animation>();
		enemyMovement = GetComponent<eMovement>();
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	void Update () {
		
		if(player != null){
			distance = Vector2.Distance(player.transform.position,this.transform.position);
		}

		if(distance <= distanceFromPlayer && timer >= timeBetweenAttacks && !enemyMovement.IsStun){
			
			sAnimation["SkeletonAttack"].speed = 0.1f;
			sAnimation.Play("SkeletonAttack");
			isAttacking = true;	
			timer = 0;		
			
		}
		if(!sAnimation.isPlaying)
			isAttacking = false;

		timer += Time.deltaTime;
		enemyMovement.Attacking(sAnimation.isPlaying);
	}

	public void OnCollisionEnter2DChild(Collider2D other)
	{
		if(other != null){
			if(other.gameObject.tag == "Player" && sAnimation.isPlaying&&isAttacking){
				other.gameObject.GetComponent<Health>().pAttacked(damage);
				isAttacking = false;
			}
		}
	}

}
