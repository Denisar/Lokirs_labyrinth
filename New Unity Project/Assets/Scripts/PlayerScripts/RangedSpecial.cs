﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedSpecial : MonoBehaviour {

	[SerializeField]
	private int damage;
	[SerializeField] 
	private int speed;
	[SerializeField]
	private GameObject player;
	private bool isFacingRight;
	
	void Awake()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		isFacingRight = player.GetComponent<pMovement>().Facing();
	}
	void FixedUpdate()
	{
		if(isFacingRight){
			transform.Translate(Vector2.right*speed*Time.deltaTime);
			transform.localScale = new Vector3(1,transform.localScale.y,transform.localScale.z);
		}else{
			transform.localScale = new Vector3(-1,transform.localScale.y,transform.localScale.z);
			transform.Translate(Vector2.left*speed*Time.deltaTime);
		}
		Destroy(gameObject,3f);
	}


	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Enemy"){
			Health enemyHealth = other.GetComponent<Health>();
			enemyHealth.pAttacked(damage);
			Destroy(gameObject);
		}
		if(other.tag == "Ground"||other.tag == "Platform"){
			Destroy(gameObject);
		}
	}
}
