﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour {

	private int selectedWeapon = 0;
	private Reset blockReset;

	void Start () {
		SelectWeapon();
		blockReset = this.GetComponentInParent<Reset>();
	}

	public int SelectedWeapon{get{return selectedWeapon;}}

	void Update () {
		int previousSelectedweapon = selectedWeapon;

		if(Input.GetKeyDown(KeyCode.Alpha1)){
			selectedWeapon = 0;
		}
		if(Input.GetKeyDown(KeyCode.Alpha2)){
			selectedWeapon = 1;
		}
		if(Input.GetKeyDown(KeyCode.Alpha3)){
			selectedWeapon = 2;
		}
		if(previousSelectedweapon != selectedWeapon){
			SelectWeapon();
			blockReset.reset();
		}
	}

	void SelectWeapon(){
		int i = 0;
		foreach(Transform weapon in transform){
			if(i == selectedWeapon){
				weapon.GetComponent<SpriteRenderer>().enabled = true;
			}else{
				weapon.GetComponent<SpriteRenderer>().enabled = false;
			}
			i++;
		}
	}
}
