﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset : MonoBehaviour {

	private pMovement playerMovement;

	void Start()
	{
		playerMovement = this.GetComponent<pMovement>();
	}

	public void reset(){
		playerMovement.pBlocking = false;
	}
}
