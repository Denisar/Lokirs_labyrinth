﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundedGreatSword : MonoBehaviour {

	void Update () {
		GameObject.Destroy(this.gameObject,30f);
		GameObject[] isOnScene = (GameObject.FindGameObjectsWithTag("GGS"));
		if(isOnScene.Length >= 2){
		GameObject.Destroy(isOnScene[0]);
		}
	}
	void OnTriggerExit2D(Collider2D other)
	{
		if(other.gameObject.tag == "Rope")
		GameObject.Destroy(this.gameObject);
	}
}
