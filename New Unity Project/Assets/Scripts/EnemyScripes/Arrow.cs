﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {

	[SerializeField]
	private int damage = 10;

	private float speed = 10;
	private bool isFacingRight;
	private bool finalRotation;
	private bool hitPlayer = true;

	public void Facing(bool _isFacingRight){
		if(finalRotation == false){
		isFacingRight = _isFacingRight;
		finalRotation = true;
		}
	}
	void FixedUpdate()
	{
		if(isFacingRight){
		transform.localScale = new Vector3(0.119933f,transform.localScale.y,transform.localScale.z);
		transform.Translate(Vector2.right*speed*Time.deltaTime);
		}else{
		transform.localScale = new Vector3(-0.119933f,transform.localScale.y,transform.localScale.z);
		transform.Translate(Vector2.left*speed*Time.deltaTime);
		}
		GameObject.Destroy(this.gameObject,3);
	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == "Player" && hitPlayer == true){
			other.gameObject.GetComponent<Health>().pAttacked(damage);
			GameObject.Destroy(this.gameObject);
		}
		if(other.gameObject.tag == "Platform"||other.gameObject.tag == "Ground"){
			GameObject.Destroy(this.gameObject);
		}
		if(other.gameObject.tag == "Shield"){
			if(isFacingRight){
				isFacingRight = false;
				hitPlayer = false;
			}else{
				isFacingRight = true;
				hitPlayer = false;
			}
		}
		if(other.gameObject.tag == "Enemy" && hitPlayer == false){
			other.GetComponent<Health>().pAttacked(damage);
			GameObject.Destroy(this.gameObject);
		}
	}
}
