﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShockWaveHit : MonoBehaviour {
	
	[SerializeField]
	private int damage;
	private int speed = 10;
	private bool isGoingRight;

	public bool IsGoingRight{set{isGoingRight = value;}}
	
	void Update () {
		if(isGoingRight){
			transform.Translate(Vector2.right*speed*Time.deltaTime);
		}else{
			transform.localScale = new Vector3(-0.3396783f,transform.localScale.y,transform.localScale.z);
			transform.Translate(Vector2.left*speed*Time.deltaTime);
		}
		GameObject.Destroy(this.gameObject,3);
	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == "Player"){
			other.GetComponent<Health>().pAttacked(damage);
			GameObject.Destroy(this.gameObject);
		}
		if(other.gameObject.tag == "Platform"||other.gameObject.tag == "Wall"){
			GameObject.Destroy(this.gameObject);
		}
	}
	public void DirectionPicker(bool isRight){
		isGoingRight = isRight;
	}
}
