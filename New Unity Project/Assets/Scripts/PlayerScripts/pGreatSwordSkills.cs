﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pGreatSwordSkills : MonoBehaviour {

	[SerializeField]
	private Transform specialPrefab;
	[SerializeField]
	private Transform Player;
	[SerializeField]
	private Transform rope;
	[SerializeField]
	private Transform groundedGreatSword;
	[SerializeField]
	private GameObject ShockWave;
	[SerializeField]
	private Slider SASlider;

	private int swordFacing;
	private int swordRotation;
	private float ropeFacing;
	private Animation sAnimation;
	private Health Health;
	private pMovement playerMovement;
	private Rigidbody2D parentRB;
	private Collider2D pCollider;
	private eMovement enemyMovemnet;
	private SpriteRenderer spriteRenderer;
	private int damage = 10;
	private int facingDirection = 2;
	private float ropeDirection = 3.2f;
	private int swordAngle = 28;
	private int knockbackStrength = 3;
	private float timeBetweenAttacks = 0.5f;
	private float timeBetweenSpecialAttacks = 10f;
	private float attackTimer = 0.5f;
	private float specialTimer = 1f;
	private bool isAttacking = false;

	


	void Awake () {
		playerMovement = GetComponentInParent<pMovement>();
		sAnimation = this.GetComponent<Animation>();
		pCollider = this.GetComponent<Collider2D>();
		spriteRenderer = this.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		Timers();
		if(spriteRenderer.enabled == true){
			BasicAttack();
			Defensive();
			Special();
			Mobility();
			UITimer();
			pCollider.enabled = playerMovement.pBlocking;
			pCollider.isTrigger = !playerMovement.pBlocking;
		}
	}
	public void UITimer(){
			SASlider.maxValue = timeBetweenSpecialAttacks;
			SASlider.value = specialTimer;
	}

	private void BasicAttack(){
		if(Input.GetButtonDown("Fire1")&&attackTimer >= timeBetweenAttacks){
			sAnimation.Play("SwordSwing");
			isAttacking = true;
			attackTimer = 0;
		}
	}

	private void Defensive(){
		if(Input.GetButtonDown("Fire2")){
			isAttacking = false;
			sAnimation.Play("SwordBlock");
			playerMovement.pBlocking = true;
		}else if(Input.GetButtonUp("Fire2")){
			playerMovement.pBlocking = false;
			sAnimation.Play("Default");
		}

	}

	private void Special(){
		if(Input.GetButtonDown("Fire3")&&specialTimer >= timeBetweenSpecialAttacks){
			sAnimation.Play("SpecialAttack");
			sAnimation["SpecialAttack"].speed = 1.5f;
			specialTimer = 0;
			Instantiate(specialPrefab,transform.position,transform.rotation);
		}
	}

	private void Mobility(){
		if(Input.GetButtonDown("Fire4")){
			if(playerMovement.Facing() == true){
				swordFacing = facingDirection;
				ropeFacing = ropeDirection;
				swordRotation = swordAngle;
			}else{
				swordFacing = -facingDirection;
				ropeFacing = -ropeDirection;
				swordRotation = -swordAngle;
			}
			Instantiate(groundedGreatSword,new Vector2(transform.position.x+swordFacing,transform.position.y),new Quaternion(swordRotation,90,0,0));
			Instantiate(rope,new Vector2(transform.position.x+ropeFacing,transform.position.y-0.45f),new Quaternion(0,0,0,0));
		}
	}

	private void Timers(){
		attackTimer += Time.deltaTime;
		specialTimer += Time.deltaTime;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(isAttacking){
			if(other.tag =="Enemy"){
				isAttacking = false;
				Health = other.GetComponent<Health>();
				other.GetComponent<eMovement>().KnockedBack(knockbackStrength);
				Health.pAttacked(damage);
			}
		}
	}
}
