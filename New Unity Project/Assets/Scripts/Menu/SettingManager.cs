﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class SettingManager : MonoBehaviour {
	
	public Toggle fullScreenToggle;
	public Dropdown resolutionDropDown;
	public Dropdown vSyncDropDown;
	public Slider musicVolumeSlider;
	public Button applyButton;

	public AudioSource music; //Add music to this
	public Resolution[] resolutions;
	public GameSettings gameSettings;

	void OnEnable()
	{
		gameSettings = new GameSettings();
		
		fullScreenToggle.onValueChanged.AddListener(delegate {OnFullscreenToggle(); });
		resolutionDropDown.onValueChanged.AddListener(delegate {onResolutionChange(); });
		vSyncDropDown.onValueChanged.AddListener(delegate {OnVSyncChange(); });
		musicVolumeSlider.onValueChanged.AddListener(delegate {onMusicVolumeChange(); });
		applyButton.onClick.AddListener(delegate{OnApplyButtonClick();});

		resolutions = Screen.resolutions;
		foreach(Resolution resolution in resolutions){
			resolutionDropDown.options.Add(new Dropdown.OptionData(resolution.ToString()));
		}
		LoadSettings();
	}

	public void OnFullscreenToggle(){
		gameSettings.fullScreen = Screen.fullScreen = fullScreenToggle.isOn;
	}
	public void onResolutionChange(){
		Screen.SetResolution(resolutions[resolutionDropDown.value].width,resolutions[resolutionDropDown.value].height,Screen.fullScreen);
		gameSettings.resolutionIndex = resolutionDropDown.value;
	}
	public void OnVSyncChange(){
		QualitySettings.vSyncCount = gameSettings.vSync = vSyncDropDown.value;
	}
	public void onMusicVolumeChange(){
		music.volume =  gameSettings.musicVolume = musicVolumeSlider.value;
		gameSettings.musicVolume = musicVolumeSlider.value; 
	}
	public void OnApplyButtonClick(){
		SaveSettings();
	}

	public void SaveSettings(){
		string jsonData = JsonUtility.ToJson(gameSettings,true);
		File.WriteAllText(Application.persistentDataPath + "/gameSettings.json",jsonData);
	}
	public void LoadSettings(){
		gameSettings = JsonUtility.FromJson<GameSettings>(File.ReadAllText(Application.persistentDataPath + "/gameSettings.json"));

		musicVolumeSlider.value = gameSettings.musicVolume;
		vSyncDropDown.value = gameSettings.vSync;
		resolutionDropDown.value = gameSettings.resolutionIndex;
		fullScreenToggle.isOn = gameSettings.fullScreen;
		Screen.fullScreen = gameSettings.fullScreen;
		
		resolutionDropDown.RefreshShownValue();
	}
}
