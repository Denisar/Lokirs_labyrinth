﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossDoor : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == "Player")
			other.gameObject.tag = "Untagged";
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
	}
}
