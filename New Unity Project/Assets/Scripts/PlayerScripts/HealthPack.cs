﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour {

	private int healAmount = 50;
	private bool isUsed = false;

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == "Player" && !isUsed){
			GameObject.Destroy(this.gameObject);
			isUsed = true;
			other.GetComponent<Health>().HealPlayer(healAmount);

		}
	}
}
