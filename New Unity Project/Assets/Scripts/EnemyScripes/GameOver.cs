﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

	[SerializeField]
	private Button btnMainMenu;
	[SerializeField]
	private Button btnQuit;

	void Start () {
		btnMainMenu.onClick.AddListener(MainMenu);
		btnQuit.onClick.AddListener(Quit);
	}
	
	private void MainMenu(){
		SceneManager.LoadScene("Menu Scene");
	}

	private void Quit(){
		Application.Quit();
	}
}
