﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eAttack : MonoBehaviour {

	[SerializeField]
    private float timeBetweenAttacks;
    [SerializeField]
    private int attackDamage = 10;
    
    
	private GameObject player;
    private Health playerHealth;
    private eMovement movement;
	private bool playerInRange;
	private float timer;

	
	void Awake()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		playerHealth = player.GetComponent <Health> ();
        movement = this.GetComponent<eMovement>();
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject == player  && other.tag == "Player")
        {
            playerInRange = true;
        }
	}


	void OnTriggerExit2D(Collider2D other)
	{
		if(other.gameObject == player && other.tag == "Player")
        {
            playerInRange = false;
        }
	}
	
	void Update () {
		timer += Time.deltaTime;
        if(timer >= timeBetweenAttacks && playerInRange && !movement.IsStun)
        {
            Attack ();
        }
	}
	void Attack ()
    {
        timer = 0f;
        if(playerHealth.GetHealth()>0)
        {
            playerHealth.pAttacked(attackDamage);
        }
    }
}
