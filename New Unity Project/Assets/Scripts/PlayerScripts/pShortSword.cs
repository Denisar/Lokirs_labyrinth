﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pShortSword : MonoBehaviour {

	private pMovement playerMovement;
	private Animation sAnimation;
	private SpriteRenderer spriteRenderer;
	private int damage = 2;
	private int dashDamage = 20;
	private float attackTimer;
	private float timeBetweenAttacks = 0.2f;
	private float specialAttackTimer;
	private float timeBetweenSpecialAttacks = 15f;
	private float dashDuration = 0.2f;
	public bool isAttacking;
	//private bool isParrying; needs to put in. later
	private bool isDashing;
	private bool isClimbing;
	
	[SerializeField]
	private Slider SASlider;

	void Awake()
	{
		playerMovement = GetComponentInParent<pMovement>();
		sAnimation = this.GetComponent<Animation>();
		spriteRenderer = this.GetComponent<SpriteRenderer>();
	}
	
	void Update () {
		Timers();
		if(spriteRenderer.enabled == true){
			BasicAttack();
			Defensive();
			Special();
			UITimer();
		}
	}

	private void UITimer(){
		SASlider.maxValue = timeBetweenSpecialAttacks;
		SASlider.value = specialAttackTimer;
	}

	private void BasicAttack(){
		if(Input.GetButton("Fire1")&&attackTimer >= timeBetweenAttacks){
			sAnimation.Play("ShortSwordsAttack");
			attackTimer = 0;
		}
		isAttacking = (sAnimation.IsPlaying("ShortSwordsAttack"))? true:false;
	}

	private void Defensive(){
		if(Input.GetButton("Fire2")){
			sAnimation.Play("Parry");
			playerMovement.pBlocking = true;
			//isParrying = true;
		}else{
			playerMovement.pBlocking = false;
			//isParrying = false;
		}
	}

	private void Special(){
		if(Input.GetButtonDown("Fire3")&& specialAttackTimer >= timeBetweenSpecialAttacks){
			playerMovement.Dash();
			dashDuration = 0.4f;
			isDashing = true;
			specialAttackTimer = 0;
		}
		if(dashDuration <= 0){
			playerMovement.ResetDash();
			isDashing = false;
		}
	}

	private void Timers(){
		attackTimer += Time.deltaTime;
		specialAttackTimer += Time.deltaTime;
		dashDuration -= Time.deltaTime;
	}

	void OnTriggerEnter2D(Collider2D other)
	{	
		hitEnemy(other);
		if(other.tag ==  "Enemy" && isDashing){
			Health Health = other.GetComponent<Health>();
			Health.pAttacked(dashDamage);
		}
	}

	void OnTriggerStay2D(Collider2D other)
	{
		hitEnemy(other);
	}

	private void hitEnemy(Collider2D other){
		if(other.tag == "Enemy" && isAttacking){
			Health Health = other.GetComponent<Health>();
			other.GetComponent<eMovement>().KnockedBack(1);
			Health.pAttacked(damage);
		}
	}

	
}
