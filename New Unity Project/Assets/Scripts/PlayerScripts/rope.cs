﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rope : MonoBehaviour {

	[SerializeField]
	private pMovement playerMovement;
	[SerializeField]
	private Vector2 newRope;
	[SerializeField]
	private Vector2 ropelocation;
	[SerializeField]
	private SpriteRenderer spriteRenderer;
	[SerializeField]
	private BoxCollider2D ropeCollider;

	private float sizeAdjustment = 1f;
	private float positionAdjustment = 0.08f;

	private bool isGrounded = false;
	
	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer>();
		ropeCollider = GetComponent<BoxCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if(!isGrounded){
			newRope = spriteRenderer.size;
			ropelocation = transform.position;
			newRope.y += sizeAdjustment;
			ropelocation.y -= positionAdjustment;
			spriteRenderer.size = newRope;
			transform.position = ropelocation;
			ropeCollider.size = new Vector2(21,Mathf.Abs(newRope.y));
		}

		GameObject.Destroy(this.gameObject,30f);
		GameObject[] isOnScene = (GameObject.FindGameObjectsWithTag("Rope"));
		if(isOnScene.Length >= 2){
			GameObject.Destroy(isOnScene[0]);
		}

	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == "Ground" || other.gameObject.tag == "Platform"){
			isGrounded = true;
		}
	}
}
