﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eMovement : MonoBehaviour {

	[SerializeField] private float speed = 1f;
	[SerializeField] private float range;
	[SerializeField] private int ViewDistance;
	[SerializeField] private float scaling;
	[SerializeField]
	private float KnockBackRange;
	private Health enemyHealth;
	private Transform tGround;
	private GameObject player;
	private Rigidbody2D eRB;
	private float randomLocation;
	private float movementTimer;
	private float maxMovemnetTimer=40;
	private float kncokbackTimer;
	private float maxKnockBackTimer=10f;
	private float facingDirection;
	private float stunDuration;
	private float pullSpeed;
	private bool isGrounded;
	private bool isPulled;
	private bool isAttacking;
	private bool isFacingRight;
	private bool isArcherFiringRange = false;
	private bool isStun;

	public LayerMask eMask;
	
	public bool IsStun{get{return isStun;}set{isStun = value;}}
	public void Stun(){
		isStun = true;
	}
	void Awake(){
		player = GameObject.FindGameObjectWithTag ("Player");
		enemyHealth = gameObject.GetComponent<Health>();
		eRB = gameObject.GetComponent<Rigidbody2D>();
		tGround = GameObject.Find(this.name+"/tGround").transform;
	}
	
	void FixedUpdate(){
		if(player == null){
			GameObject.Destroy(this.gameObject,0f);
		}
		facingDirection = player.transform.position.x-transform.position.x;
		if(!enemyHealth.Status() && isGrounded && kncokbackTimer<=0 && !isStun && !isAttacking){//only moves when its alive
			float awayFromPlayer = Vector2.Distance(player.transform.position,transform.position);
			if(awayFromPlayer<ViewDistance){
				Move((Mathf.Sign(facingDirection)),speed+3);
			}else{
				if(movementTimer <=0){
					movementTimer = maxMovemnetTimer;
					randomLocation = Random.Range(-1,1);
				}
				Move(Mathf.Sign(randomLocation),speed);
			}
		}
		if(Mathf.Abs(facingDirection) <= 1 && isGrounded && kncokbackTimer<=0 && !isPulled){
			Move(0,0);
			
		}
		if(Mathf.Abs(facingDirection) <= 1 && isPulled){
			isPulled = false;
		}else if(isPulled){
			Move(Mathf.Sign(facingDirection),pullSpeed);
		}
		if(isArcherFiringRange)
			Move(0,0);
		
	}
	void Update()
	{
		
		isGrounded = Physics2D.Linecast (transform.position,tGround.position,eMask);
		Timers();

		if(stunDuration <=0){
			isStun = false;

		}	
		if(!isAttacking){
			eRB.constraints = RigidbodyConstraints2D.FreezeRotation;
		}else{
			eRB.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionY;
		}

	}
	public void Timers(){
		if(movementTimer>=0)
		movementTimer--;

		if(kncokbackTimer>=0)
		kncokbackTimer--;

		if(isStun){
			stunDuration -= Time.deltaTime;
		}
	}
	public float distance(){
		return ViewDistance;
	}
	public void inFiringRange(bool _isArhcerFiringRange){
		isArcherFiringRange = _isArhcerFiringRange;

	}

	private void Move(float h,float speed){
		Vector2 pVelocity = eRB.velocity;
		pVelocity.x = h * speed;
		eRB.velocity = pVelocity;
		Flip(h);
	}
	private void Flip(float pFacing){
		if(pFacing < -0.1){
			transform.localScale = new Vector3(-scaling,transform.localScale.y,transform.localScale.z);
			isFacingRight = false;
		}else if(pFacing > 0.1){
			transform.localScale = new Vector3(scaling,transform.localScale.y,transform.localScale.z);
			isFacingRight = true;
		}
	}
	public bool Facing(){
		return isFacingRight;
	}

	public void KnockedBack(float rangeModifier){
		kncokbackTimer = maxKnockBackTimer;
		eRB.velocity = new Vector2(Mathf.Sign(facingDirection)*-KnockBackRange*rangeModifier,KnockBackRange);
	}
	
	public void Stunned(float _stunTime){
		isStun = true;
		stunDuration = _stunTime;
	}
	public void pulled(float _speed){
		pullSpeed = _speed;
		isPulled = true;
	}
	public void Attacking(bool _isAttacking){
		isAttacking = _isAttacking;
	}
	 void OnCollisionEnter2D(Collision2D other)
	 {
		 if(other.gameObject.tag == "Enemy"){
			 Move(0,0);
		 }
	 }

	 void OnTriggerEnter2D(Collider2D other)
	 {
		 if(other.gameObject.tag == "LeftEdge"){
			 randomLocation = 1;
			 movementTimer = maxMovemnetTimer;
		 }
		 if(other.gameObject.tag == "RightEdge"){
			 randomLocation = -1;
			  movementTimer = maxMovemnetTimer;
		 }
	 }	 
	 
}