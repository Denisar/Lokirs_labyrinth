﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherAttack : MonoBehaviour {

	private eMovement enemyMovement;
	private GameObject player;
	private RaycastHit2D  hit2D;
	private Vector2 Facing;
	private float distance;
	private float timer;
	private float timeBetweenAttacks = 1f;
	private float range = 30;

	[SerializeField]
	private GameObject Arrow;
	[SerializeField]
	private LayerMask mask;
	[SerializeField]
	private float attackDistance;
	

	void Start () {
		enemyMovement = GetComponent<eMovement>();
		
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		Facing = (enemyMovement.Facing() == true)? Vector2.right:Vector2.left;
		if(player != null)
			distance = Vector2.Distance(player.transform.position,this.transform.position);
			
		if(distance <= attackDistance&& timer >= timeBetweenAttacks && !enemyMovement.IsStun){
			hit2D = Physics2D.Raycast(transform.position,Facing,range,mask);
			if(hit2D.collider != null){
				if(hit2D.collider.gameObject.tag == "Player"){
					GameObject shotArrow = Instantiate(Arrow,new Vector2(this.transform.position.x,this.transform.position.y +0.3f),Arrow.transform.rotation);
					shotArrow.GetComponent<Arrow>().Facing(enemyMovement.Facing());			
					timer = 0;
				}
			}
			
		}
		if(distance <= attackDistance)
			enemyMovement.inFiringRange(true);
		else	
			enemyMovement.inFiringRange(false);

		Timers();
	}
	private void Timers(){
		timer += Time.deltaTime;
	}
	
}
