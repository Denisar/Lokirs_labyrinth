﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {


	public GameObject player;

	private Vector3 offset;

	private bool changingRoom;

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		
	}
	

	// Use this for initialization
	void Start () {
		//Create the offset value for the camera use. 
		offset = transform.position - player.transform.position;
	}
	
	//Has to be done at the end, after the players position has been detected. 
	void LateUpdate () {
		if (changingRoom == false) {
		transform.position = player.transform.position + offset;
		}
	}

	public void roomChange(){
		//Fade to black?
	}
}