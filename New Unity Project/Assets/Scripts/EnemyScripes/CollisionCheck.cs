﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionCheck : MonoBehaviour {

	[SerializeField]
	private GameObject Parent;

	void OnTriggerEnter2D(Collider2D other)
	{
		if(Parent.gameObject.name == "Minotaur"){
			Parent.GetComponent<MinotaurAttack>().OnCollisionEnter2DChild(other);
		}else if(Parent.gameObject.name == "Skeleton"){
			Parent.GetComponent<SkeletonAttack>().OnCollisionEnter2DChild(other);
		}
	}
	void OnTriggerStay2D(Collider2D other)
	{
		if(Parent.gameObject.name == "Minotaur"){
			Parent.GetComponent<MinotaurAttack>().OnCollisionEnter2DChild(other);
		}else if(Parent.gameObject.name == "Skeleton"){
			Parent.GetComponent<SkeletonAttack>().OnCollisionEnter2DChild(other);
		}
	}
	

}
