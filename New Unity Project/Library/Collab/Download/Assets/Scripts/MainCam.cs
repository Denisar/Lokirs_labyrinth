﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCam : MonoBehaviour {

	public GameObject player;
	private Vector3 offset;
	public float smoothspeed = 0.125f;

	void Awake()
	{
		offset = transform.position - player.transform.position;
		this.transform.position = player.transform.position+offset;
	}

	
	void FixedUpdate()
	{
		Vector3 desiredPos = player.transform.position+offset;
		Vector3 smoothedPos = Vector3.Lerp(transform.position,desiredPos,smoothspeed);
		this.transform.position = smoothedPos;
	}
}

