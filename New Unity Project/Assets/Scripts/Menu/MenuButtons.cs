﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour {
	[SerializeField]
	private Button btnPlay;
	[SerializeField]
	private Button btnSettings;
	[SerializeField]
	private Button btnQuit;
	

	void Start () {
		btnPlay.onClick.AddListener(PlayPressed);
		btnQuit.onClick.AddListener(QuitPressed);
	}
	
	void PlayPressed(){
		SceneManager.LoadScene("Level 1");
	}
	void QuitPressed(){
		Application.Quit();
	}
}
