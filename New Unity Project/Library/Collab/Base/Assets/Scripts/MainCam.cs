﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCam : MonoBehaviour {

	public GameObject player;
	private Vector3 offset;
	public Vector3 aboveGround;
	public float smoothspeed = 0.125f;
	private int min = 10;

	void Awake()
	{
		offset = transform.position - player.transform.position;
		this.transform.position = player.transform.position+offset;
	}

	
	void FixedUpdate()
	{
		
		Vector3 desiredPos = player.transform.position+offset;
		Vector3 smoothedPos = Vector3.Lerp(transform.position,desiredPos,smoothspeed);
		this.transform.position = smoothedPos;
	}
}

