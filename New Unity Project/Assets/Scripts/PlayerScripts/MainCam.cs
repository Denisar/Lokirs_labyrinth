﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCam : MonoBehaviour {

	private GameObject player;
	private Vector3 offset;
	[SerializeField]
	private float smoothspeed = 0.125f;
	

	void Awake()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		offset = transform.position - player.transform.position;
		this.transform.position = player.transform.position+offset;
	}

	
	void FixedUpdate()
	{
		if(player != null){
			Vector3 desiredPos = player.transform.position+offset;
			Vector3 smoothedPos = Vector3.Lerp(transform.position,desiredPos,smoothspeed);
			this.transform.position = smoothedPos;
			if(this.transform.position.y <15){
				this.transform.position = new Vector3(this.transform.position.x,15,-10);
			}
			if(this.transform.position.x<-39){
				this.transform.position = new Vector3(-39,this.transform.position.y,-10);
			}
		}
	}
}

