﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {
	[SerializeField] int fireOneDamage = 20;//The gun damage
	[SerializeField] int fireTwoDamage = 10;//The gun damage
	[SerializeField] float fireRate = 0.3f;//how often the gun can fire
	[SerializeField] float altFireRate = 1f;
	[SerializeField] float abilityOneCoolDown = 1f;
	[SerializeField] float abilityTwoCoolDown = 1f;

	public Transform BulletTrailPrefab;//The bullet prefab
	float Shottimer;//Timer used to calculate time between shots
	float altFireRateTimer;
	float abilityOneTimer;
	float abilityTwoTimer;
	Transform firePoint;//Position of the gun
	PlayerAbility playerAbility;
	void Awake()
	{
	
		playerAbility = GetComponent<PlayerAbility>();
		firePoint = transform.Find("Gun");//Locating the gun on the character
		if(firePoint == null){//Debug to make sure that the gun exists on the player
			Debug.Log("No firepoint");
		}
	}

	
	
	void FixedUpdate () {
		Shottimer += Time.deltaTime;//real time timer
		altFireRateTimer += Time.deltaTime;
		abilityOneTimer += Time.deltaTime;
		abilityTwoTimer += Time.deltaTime;
		if(Input.GetButton ("Fire1") && Shottimer >= fireRate && Time.timeScale != 0)//checks if gun is fired and makes sure that it is able to at the time.
        {
            Shoot (fireOneDamage);
        }
		if(Input.GetButton ("Fire2") && altFireRateTimer >= altFireRate && Time.timeScale != 0)//checks if gun is fired and makes sure that it is able to at the time.
        {
            Shoot (fireTwoDamage);
        }
		if(Input.GetButton("Ability1") && abilityOneTimer >= abilityOneCoolDown && Time.timeScale != 0){
			playerAbility.AstralLeap();
			abilityOneTimer = 0f;
		}
		if(Input.GetButton("Ability2") && abilityTwoTimer >= abilityTwoCoolDown && Time.timeScale != 0){
			playerAbility.Absorb();
			abilityTwoTimer = 0f;
		}
	}


	void Shoot(int dmg){
		Shottimer = 0f;//This is resetting the timer between shots 
		Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,Camera.main.ScreenToWorldPoint(Input.mousePosition).y);//Where the mouse is when clicked
		Vector2 firePointPosition = new Vector2(firePoint.position.x,firePoint.position.y);//The location of the gun
		Effect(dmg);//Spawns the bullet instance
		Debug.DrawLine (firePointPosition,(mousePosition-firePointPosition)*100,Color.red);//This is the debug line to see where the player is aiming
	}
	void Effect(int dmg){
		Transform thisbullet = Instantiate(BulletTrailPrefab,firePoint.position,firePoint.rotation);//Spawns the instance of the bullet
		thisbullet.GetComponent<Bullet>().SetDamage(dmg);//This will make it so that the bullet does damage not the player
		thisbullet.GetComponent<Bullet>().SetFirer(gameObject);//This is sent so that we know how shot the bullet
		
	}
}
