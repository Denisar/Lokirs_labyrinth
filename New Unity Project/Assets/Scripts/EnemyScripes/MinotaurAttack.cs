﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinotaurAttack : MonoBehaviour {

	private GameObject player;
	private eMovement enemyMovement;
	private Animation sAnimation;
	private float timer;
	private float rangedTimer;
	private float distance;
	private bool isAttacking;
	private bool canPickAttack = true;
	private bool specialAttack;
	private int whichAttack;
	private int damage;

	[SerializeField] 
	private float distanceFromPlayer;
	[SerializeField]
	private GameObject ShockWave;
	[SerializeField]
	private int normalAttackDamage = 20;
	[SerializeField]
	private int chargeAttackDamage = 100;
	[SerializeField]
	private float timeBetweenAttacks;
	[SerializeField]
	private float timeBetweenRangedAttacks = 15f;
	[SerializeField]
	private float attackSpeed;
	[SerializeField]
	private float RangedAttackDistance = 20;

	void Start () {
		sAnimation = GetComponent<Animation>();
		enemyMovement = GetComponent<eMovement>();
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	void FixedUpdate () {
		distance = Vector2.Distance(player.transform.position,this.transform.position);
		AttackChecker();
		Attack(whichAttack);
		RangedAttack();
		Timers();
	}

	private void AttackChecker(){
		if(canPickAttack == true){
			canPickAttack = false;
			whichAttack = Random.Range(0,2);
		}
		if(!sAnimation.isPlaying){
			isAttacking = false;
		}
		enemyMovement.Attacking(sAnimation.isPlaying);
	}

	private void RangedAttack(){
		if(distanceFromPlayer <= distance && distance <= RangedAttackDistance  && rangedTimer >= timeBetweenRangedAttacks && !enemyMovement.IsStun){
			sAnimation.Play("MinotaurShockWave");
			rangedTimer = 0;
			timer = 0;
			specialAttack = true;
		} 
		if(sAnimation["MinotaurShockWave"].time >= 1.7f && specialAttack){
			specialAttack = false;
			shockWaveAttack();
		}
	}
	private void shockWaveAttack(){
			GameObject RightWave = Instantiate(ShockWave,new Vector2(this.transform.position.x,this.transform.position.y-0.5f),ShockWave.transform.rotation);
			RightWave.GetComponent<ShockWaveHit>().DirectionPicker(true);
			GameObject LeftWave = Instantiate(ShockWave,new Vector2(this.transform.position.x,this.transform.position.y-0.5f),ShockWave.transform.rotation);
			LeftWave.GetComponent<ShockWaveHit>().DirectionPicker(false);
	}
	private void Attack(int pickedAttack){
		if(distance <= distanceFromPlayer && timer >= timeBetweenAttacks && !enemyMovement.IsStun && pickedAttack == 0){
			damage = normalAttackDamage;
			sAnimation["MinotaurAttack"].speed = attackSpeed;
			sAnimation.Play("MinotaurAttack");
			AttackReset();
		}else if(distance <= distanceFromPlayer && timer >= timeBetweenAttacks && !enemyMovement.IsStun && pickedAttack == 1){
			damage = chargeAttackDamage;
			sAnimation["MinotaurChargeAttack"].speed = attackSpeed;
			sAnimation.Play("MinotaurChargeAttack");
			AttackReset();
		}
	}
	public void AttackReset(){
			timer = 0;
			isAttacking = true;
			canPickAttack = true;
	}

	private void Timers(){
		timer += Time.deltaTime;
		rangedTimer += Time.deltaTime;
	}
	public void OnCollisionEnter2DChild(Collider2D other)
	{
		if(other != null){
			if(other.gameObject.tag == "Player" && sAnimation.isPlaying&&isAttacking){
				other.gameObject.GetComponent<Health>().pAttacked(damage);
				isAttacking = false;
			}
		}
	}

}
