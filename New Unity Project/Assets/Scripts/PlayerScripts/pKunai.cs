﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pKunai : MonoBehaviour {

	private static Vector2 startLocation = new Vector2(5.096123f,-0.8113248f);
	private Vector2 mouseDirection;
	private Vector3 targetPos;
	private SpriteRenderer sR;
	private pMovement playerMovement;
	private LineRenderer lineRenderer;
	private int damage = 5;
	private int xDistanceFromPlayer = 3;
	private int yDistanceFromPlayer = 2;
	private bool isMaxRange;
	private bool specialAttack;
	private bool canAttack;
	private bool isAttacking;
	private bool isGrappling = false;
	private bool canGrapple = true;
	private bool isHooked;
	private bool isGrappleMaxRange;
	private float timeBetweenAttacks =0.5f;
	private float attackTimer =0.4f;
	private float timeBetweenSpecialAttacks = 5f;
	private float specialAttackTimer = 0f;
	private float mobilityTimer =0;
	private float timeBetweenMobilityTimer = 0.5f;
	private Quaternion defRotation; 

	[SerializeField]
	private Transform Rope;
	[SerializeField]
	private GameObject shield;
	[SerializeField]
	private Slider SASlider;
	[SerializeField]
	private LayerMask mask;
	[SerializeField]
	private float maxRange;
	[SerializeField]
	private int speed;

	void Awake()
	{
		playerMovement = GetComponentInParent<pMovement>();
		lineRenderer = GetComponent<LineRenderer>();
		sR = GetComponent<SpriteRenderer>();
		startLocation = this.transform.localPosition;
		defRotation = transform.rotation;
	}

	void Update () {
		Timers();
		if(sR.enabled == true){
			BasicAttack();
			Defensive();
			Special();
			Mobility();
			reset();
			Chain();
			UITimer();
		}else{
			shield.SetActive(false);
			lineRenderer.enabled = false;
		}
	}

	private void BasicAttack(){
		isMaxRange = (Vector2.Distance(playerMovement.transform.position,this.transform.position) >= maxRange)? true : false;
		if(Input.GetButton("Fire1") && canAttack && attackTimer >= timeBetweenAttacks && !playerMovement.pBlocking && !isMaxRange){
			transform.Translate(Vector2.right*speed*Time.deltaTime);
			isAttacking = true;
		}else if((isMaxRange||Input.GetButtonUp("Fire1") || canAttack == false) && !isGrappling){
			isAttacking = false;
			canAttack = false;
			attackTimer = 0;
		}
	}

	private void Defensive(){
		if(Input.GetButtonDown("Fire2") && !isAttacking){
			shield.SetActive(true);
			shield.transform.localPosition = new Vector3(startLocation.x-xDistanceFromPlayer,startLocation.y+yDistanceFromPlayer,0);
			lineRenderer.SetPosition(1,shield.transform.position);
			playerMovement.pBlocking = true;
		}else if(Input.GetButtonUp("Fire2")){
			sR.enabled = true;
			shield.SetActive(false);
			playerMovement.pBlocking = false;
			canAttack = true;
		}
	}

	private void Special(){
		if(Input.GetButton("Fire3")&&!isMaxRange&&canAttack&&specialAttackTimer >= timeBetweenSpecialAttacks){
			transform.Translate(Vector2.right*speed*Time.deltaTime);
			specialAttack = true;
		}else if((Input.GetButtonUp("Fire3")||canAttack == false||isMaxRange) && !isGrappling && specialAttack){
			canAttack = false;
			specialAttack = false;
			specialAttackTimer = 0f;

		}
	}

	private void Mobility(){
		if(Input.GetButtonDown("Fire4") && canGrapple&& !isGrappleMaxRange){
			targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			targetPos.z = 0;
		}
		if(Input.GetButton("Fire4") && canGrapple && !isGrappleMaxRange && mobilityTimer >= timeBetweenMobilityTimer){
			transform.position = Vector2.MoveTowards(transform.position,(Vector2)(targetPos),speed*Time.deltaTime);
			isGrappling = true;
			var dir = targetPos - transform.position;
			var angleDirection = Mathf.Atan2(dir.y,dir.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.AngleAxis(angleDirection, Vector3.forward);
			playerMovement.pBlocking = true;
			
		}else if((!Input.GetButton("Fire4") && isGrappling) || isGrappleMaxRange || !canGrapple||Vector2.Distance(targetPos,transform.position) <= 1){
			transform.localPosition = startLocation;
			isGrappling = false;
			mobilityTimer = 0f;
			transform.rotation = defRotation;
			playerMovement.pBlocking = false;
		}
	}

	private void UITimer(){
		SASlider.maxValue = timeBetweenSpecialAttacks;
		SASlider.value = specialAttackTimer;
	}

	private void reset(){
		if(!canAttack && Vector2.Distance(transform.localPosition, startLocation) >= 2){
			transform.Translate(Vector2.left*speed*Time.deltaTime);
		}else{
			canGrapple = true;
			canAttack = true;
		}
		playerMovement.Attacking = isAttacking;
	}
	private void Chain(){
		lineRenderer.SetPosition(0,transform.parent.position);
		lineRenderer.SetPosition(1,this.transform.position);
		lineRenderer.enabled = true;
		isGrappleMaxRange = (Vector2.Distance(playerMovement.transform.position,this.transform.position) >= maxRange+20)? true : false;
	}

	private void Timers(){
		attackTimer += Time.deltaTime;
		specialAttackTimer += Time.deltaTime;
		mobilityTimer += Time.deltaTime;
	}

	void AttackCheck(Collider2D other){
		if(isGrappling && other.gameObject.tag == "Ledge" ){
			Instantiate(Rope,new Vector2(other.transform.position.x + 0.2f,other.transform.position.y-3.5f),new Quaternion(0,0,0,0));
		}
		if(other.gameObject.tag == "Enemy"){
			if(specialAttack){
				eMovement enemyMovement = other.GetComponent<eMovement>();
				enemyMovement.Stunned(3f);
				enemyMovement.pulled(speed);
				canAttack = false;
			}
			if(isAttacking){
				isAttacking = false;
				Health Health = other.GetComponent<Health>();
				other.GetComponent<eMovement>().KnockedBack(0);
				Health.pAttacked(damage);
			}
		}

		if(other.tag == "Ground"||other.tag == "Platform" || other.tag == "Ledge"){
			canAttack = false;
			canGrapple = false;
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		AttackCheck(other);

	}

	void OnTriggerStay2D(Collider2D other)
	{
		AttackCheck(other);
	}
	
}
